package reir2180MV.repository;

import reir2180MV.model.Intrebare;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class QuizRepository implements IRepository<Intrebare> {

    private List<Intrebare> listQuestions;

    public QuizRepository() {
        setIntrebari(new LinkedList<Intrebare>());
    }

    @Override
    public void add2(Intrebare entity) throws Exception {
        if (exists(entity)) {
            throw new Exception("Question already in quiz!");
        }
        listQuestions.add(entity);
        String fileName = "intrebari3.txt";
        FileWriter fileWriter = new FileWriter(fileName, true);
        fileWriter.write(entity.getEnunt() + "\n");
        fileWriter.write(entity.getVarianta1() + "\n");
        fileWriter.write(entity.getVarianta2() + "\n");
        fileWriter.write(entity.getVarianta3() + "\n");
        fileWriter.write(entity.getVariantaCorecta() + "\n");
        fileWriter.write(entity.getDomeniu() + "\n");
        fileWriter.write("##" + "\n");
        fileWriter.close();
    }

    @Override
    public Intrebare add(Intrebare entity) throws Exception {
        return null;
    }

    @Override
    public Iterable<Intrebare> getAll() {
        return null;
    }

    @Override
    public boolean exists(Intrebare obj) {
        for (Intrebare intrebare : listQuestions)
            if (intrebare.equals(obj))
                return true;
        return false;
    }

    public Intrebare pickRandomQuiz() {
        Random random = new Random();
        return listQuestions.get(random.nextInt(listQuestions.size()));
    }

    public int getNumberOfDistinctsDomains() {
        return getDistinctDomain().size();
    }

    public Set<String> getDistinctDomain() {
        Set<String> domain = new TreeSet<>();
        for (Intrebare intrebare : listQuestions)
            domain.add(intrebare.getDomeniu());
        return domain;
    }

    public List<Intrebare> getIntrebareByDomain(String domain) {
        List<Intrebare> intrebares = new LinkedList<>();
        for (Intrebare intrebare : listQuestions) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebares.add(intrebare); }
        }
        return intrebares;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebareByDomain(domain).size();
    }

    public List<Intrebare> getQuestion() {
        return listQuestions;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.listQuestions = intrebari;
    }

    public List<Intrebare> loadQuestionFromFile(String filenam) {
        List<Intrebare> intrebares = new LinkedList<>();
        BufferedReader bufferedReader = null;
        String line = null;
        List<String> intrebariAux;
        Intrebare intrebare;
        try {
            bufferedReader = new BufferedReader(new FileReader(filenam));
            line = bufferedReader.readLine();
            while (line != null) {
                intrebariAux = new LinkedList<>();
                while (!line.equals("##")) {
                    intrebariAux.add(line);
                    line = bufferedReader.readLine();
                }
                intrebare = new Intrebare();
                intrebare.setEnunt(intrebariAux.get(0));
                intrebare.setVarianta1(intrebariAux.get(1));
                intrebare.setVarianta2(intrebariAux.get(2));
                intrebare.setVarianta3(intrebariAux.get(3));
                intrebare.setVariantaCorecta(Integer.parseInt(intrebariAux.get(4)));
                intrebare.setDomeniu(intrebariAux.get(5));
                intrebares.add(intrebare);
                line = bufferedReader.readLine();
            }
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return intrebares;
    }
}
