package reir2180MV.repository;

import reir2180MV.validator.IValidator;

import java.util.ArrayList;
import java.util.List;

public class MemoryRepository<E> implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();
    protected IValidator<E> vali;

    public MemoryRepository(IValidator<E> v) {
        vali = v;
    }

    @Override
    public E add(E entity) throws Exception {
        vali.validate(entity);
        for (E ent : entities) {
            if (entity.equals(ent))
                throw new Exception("Exista deja in fisierul intrebari.txt!");
        }
        entities.add(entity);
        return null;
    }

    @Override
    public Iterable<E> getAll() {
        return entities;
    }

    @Override
    public boolean exists(E obj) {
        return false;
    }

    @Override
    public void add2(E obj) {

    }

}
