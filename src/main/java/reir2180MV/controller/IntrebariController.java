package reir2180MV.controller;

import java.util.ArrayList;
import java.util.List;

import reir2180MV.exception.DuplicateIntrebareException;
import reir2180MV.exception.InputValidationFailedException;
import reir2180MV.model.Intrebare;
import reir2180MV.model.Statistica;
import reir2180MV.model.Test;
import reir2180MV.repository.IntrebariRepositoryFile;
import reir2180MV.exception.NotAbleToCreateStatisticsException;
import reir2180MV.exception.NotAbleToCreateTestException;
import reir2180MV.validator.IntrebareValidator;

public class IntrebariController {

    private IntrebariRepositoryFile intrebariRepository;

    public IntrebariController() {
        //intrebariRepository = new IntrebariRepositoryFile(new IntrebareValidator(), "src/main/resources/intrebari.txt");
        loadQuestionsFromFile("src/main/resources/intrebari.txt");
    }

    public IntrebariController(String filename) {
        loadQuestionsFromFile(filename);
    }

    public Intrebare addNewIntrebare(Intrebare intrebare) throws InputValidationFailedException {

        try {
            intrebariRepository.add(intrebare);
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        }

        return intrebare;
    }

    public void loadQuestionsFromFile(String filename) {
        intrebariRepository = new IntrebariRepositoryFile(new IntrebareValidator(), filename);
        intrebariRepository.loadIntrebariFromFile();
    }

    public boolean exists(Intrebare intrebare) {
        return intrebariRepository.exists(intrebare);
    }

    public Test createNewTest() throws NotAbleToCreateTestException {

        if (intrebariRepository.getIntrebari().size() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

        List<Intrebare> testIntrebari = new ArrayList<>();
        List<String> domenii = new ArrayList<>();
        Intrebare intrebare;
        Test test = new Test();

        while (testIntrebari.size() != 5) {
            intrebare = intrebariRepository.pickRandomIntrebare();

            if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
                testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }

        }

        test.setIntrebari(testIntrebari);
        return test;

    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nici o intrebare!");

        Statistica statistica = new Statistica();
        for (String domeniu : intrebariRepository.getDistinctDomains()) {
            int numarDeIntrebariPerDomeniu = 0;
            for (Intrebare intr : intrebariRepository.getIntrebari()) {
                if (intr.getDomeniu().equals(domeniu))
                    numarDeIntrebariPerDomeniu++;
            }
            statistica.add(domeniu, numarDeIntrebariPerDomeniu);
        }

        return statistica;
    }

}

