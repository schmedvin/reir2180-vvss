package reir2180MV.validator;

public interface IValidator<T> {
    void validate(T entity) throws Exception;
}


