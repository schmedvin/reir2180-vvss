package reir2180MV.validator;

import reir2180MV.exception.InputValidationFailedException;
import reir2180MV.model.Intrebare;

public class IntrebareValidator implements IValidator<Intrebare> {
    @Override
    public void validate(Intrebare entity) throws Exception {
        validateAll(entity.getEnunt(), entity.getVarianta1(), entity.getVarianta2(), entity.getVarianta3(), entity.getDomeniu(), entity.getVariantaCorecta());
    }

    private void validateAll(String enunt, String varianta1, String varianta2, String varianta3, String domeniu, Integer variantaCorecta) throws InputValidationFailedException {

        enunt = enunt.trim();
        varianta1 = varianta1.trim();
        varianta2 = varianta2.trim();
        varianta3 = varianta3.trim();
        domeniu = domeniu.trim();

        if (enunt.equals(""))
            throw new InputValidationFailedException("Enuntul este vid!");
        else if (!Character.isUpperCase(enunt.charAt(0)))
            throw new InputValidationFailedException("Prima litera din enunt nu e majuscula!");
        else if (!String.valueOf(enunt.charAt(enunt.length() - 1)).equals("?"))
            throw new InputValidationFailedException("Ultimul caracter din enunt nu e '?'!");
        else if (enunt.length() > 100)
            throw new InputValidationFailedException("Lungimea enuntului depaseste 100 de caractere!");
        if (varianta1.equals(""))
            throw new InputValidationFailedException("Varianta1 este vida!");
        else if (!String.valueOf(varianta1.charAt(0)).equals("1") || !String.valueOf(varianta1.charAt(1)).equals(")"))
            throw new InputValidationFailedException("Varianta1 nu incepe cu '1)'!");
        else if (varianta1.length() > 50)
            throw new InputValidationFailedException("Lungimea variantei1 depaseste 50 de caractere!");
        if (varianta2.equals(""))
            throw new InputValidationFailedException("Varianta2 este vida!");
        else if (!String.valueOf(varianta2.charAt(0)).equals("2") || !String.valueOf(varianta2.charAt(1)).equals(")"))
            throw new InputValidationFailedException("Varianta2 nu incepe cu '2)'!");
        else if (varianta2.length() > 50)
            throw new InputValidationFailedException("Lungimea variantei2 depaseste 50 de caractere!");
        if (varianta3.equals(""))
            throw new InputValidationFailedException("Varianta3 este vida!");
        else if (!String.valueOf(varianta3.charAt(0)).equals("3") || !String.valueOf(varianta3.charAt(1)).equals(")"))
            throw new InputValidationFailedException("Varianta3 nu incepe cu '3)'!");
        else if (varianta3.length() > 50)
            throw new InputValidationFailedException("Lungimea variantei3 depaseste 50 de caractere!");
        if (domeniu.equals(""))
            throw new InputValidationFailedException("Domeniul este vid!");
        else if (!Character.isUpperCase(domeniu.charAt(0)))
            throw new InputValidationFailedException("Prima litera din domeniu nu e majuscula!");
        else if (domeniu.length() > 30)
            throw new InputValidationFailedException("Lungimea domeniului depaseste 30 de caractere!");
        if (variantaCorecta != 1 && variantaCorecta != 2 && variantaCorecta != 3)
            throw new InputValidationFailedException("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
    }
}
