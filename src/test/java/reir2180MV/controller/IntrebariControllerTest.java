package reir2180MV.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import reir2180MV.exception.InputValidationFailedException;
import reir2180MV.exception.NotAbleToCreateTestException;
import reir2180MV.model.Intrebare;

import static org.junit.Assert.*;

public class IntrebariControllerTest {

    private IntrebariController intrebariController;

    @Before
    public void setUp() throws Exception {
        intrebariController = new IntrebariController();
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void addIntrebareECP1() {
        Intrebare intrebare1 = new Intrebare("De la ce firma este Note10 ?", "1) Nokia", "2) Samsung", "3) Apple", 2, "Telefoane");
        try {
            intrebariController.addNewIntrebare(intrebare1);
        }
        catch (InputValidationFailedException ex) {
            //fail();
            //assertEquals("Varianta1 nu incepe cu '1)'!", ex.getMessage());
            //assertTrue(intrebariController.exists(intrebare1));
        }
        assertTrue(intrebariController.exists(intrebare1));
    }

    @Test
    public void addIntrebareECP2() {
        Intrebare intrebare2 = new Intrebare("Ce faci?", "1) Bine.", "2) Rau.", "3) Se poate si mai bine", 5, "Conversatie1");
        try {
            intrebariController.addNewIntrebare(intrebare2);
            fail();
        } catch (InputValidationFailedException expected) {
            assertEquals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}", expected.getMessage());
        }
    }

    @Test
    public void addIntrebareECP3() {
        Intrebare intrebare3 = new Intrebare("Ce faci maine?", "Bine.", "2) Rau.", "3) Se poate si mai rau.", 1, "Conversatie2");
        try {
            intrebariController.addNewIntrebare(intrebare3);
            fail();
        }
        catch (InputValidationFailedException ex) {
            assertEquals("Varianta1 nu incepe cu '1)'!", ex.getMessage());
        }
    }

    @Test
    public void addIntrebareECP4() {
        Intrebare intrebare4 = new Intrebare("Ce zi este azi?", "", "2) Rau.", "3) Fac bine.", 1, "Conversatie3");
        try {
            intrebariController.addNewIntrebare(intrebare4);
            fail();
        } catch (InputValidationFailedException expected) {
            assertEquals("Varianta1 este vida!", expected.getMessage());
        }
    }

    @Test
    public void addIntrebareECP5() {
        Intrebare intrebare5 = new Intrebare("Ce faci azi?", "1) Nimic.", "2) Laburi.", "3) Beau.", 3, "Distractie");
        try {
            intrebariController.addNewIntrebare(intrebare5);
        }
        catch (InputValidationFailedException ex) {
            fail();
        }
    }

    @Test
    public void addIntrebareBVA1() {
        Intrebare intrebare11 = new Intrebare("Ce faci vineri?", "1) Bine.", "2) Rau.", "3) Se poate si mai bine", 2, "Conversatie11");
        try {
            intrebariController.addNewIntrebare(intrebare11);
        } catch (InputValidationFailedException expected) {
            fail();
        }
    }

    @Test
    public void addIntrebareBVA2() {
        Intrebare intrebare12 = new Intrebare("Ce faci vineri?", "1) Bine.", "2) Rau.", "3) Se poate si mai bine", 5, "Conversatie12");
        try {
            intrebariController.addNewIntrebare(intrebare12);
        } catch (InputValidationFailedException expected) {
            assertEquals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}", expected.getMessage());
        }
    }

    @Test
    public void addIntrebareBVA3() {
        Intrebare intrebare13 = new Intrebare("Ce faci vineri?", "1) Bine.", "2) Rau.", "3) Se poate si mai bine", 0, "Conversatie13");
        try {
            intrebariController.addNewIntrebare(intrebare13);
        }
        catch (InputValidationFailedException ex) {
            assertEquals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}", ex.getMessage());
        }
    }

    @Test
    public void addIntrebareBVA4() {
        Intrebare intrebare14 = new Intrebare("Ce faci vineri?", "1) Bine.", "2) Rau.", "3) Se poate si mai bine", 1, "Conversatie14");
        try {
            intrebariController.addNewIntrebare(intrebare14);
        } catch (InputValidationFailedException expected) {
            fail();
        }
    }

    //Nu a fost incarcat fisierul
    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest1() throws Exception {
        IntrebariController appController = new IntrebariController("src/main/resources/intrebariInexistente.txt");
        appController.createNewTest();

    }

    //valid
    @Test
    public void createNewTest2() throws Exception{
        IntrebariController appController = new IntrebariController("src/main/resources/intrebari.txt");
        reir2180MV.model.Test q = appController.createNewTest();
        assertEquals(q.getIntrebari().size(),5);
    }
    //nu exista 5 domenii
    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest3() throws Exception{
        IntrebariController appController = new IntrebariController("src/main/resources/intrebari2.txt");
        appController.createNewTest();
    }
}
