package reir2180MV.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import reir2180MV.controller.IntrebariController;
import reir2180MV.exception.InputValidationFailedException;
import reir2180MV.model.Intrebare;

import static org.junit.Assert.*;

public class QuizRepositoryTest {

    private IntrebariController intrebariController;

    @Before
    public void setUp() throws Exception {
        intrebariController = new IntrebariController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void add1() throws Exception {
        Intrebare intrebare = new Intrebare("Ce este?","1)2","2)3","3)3",1,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNotNull(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void add2() throws Exception {
        Intrebare intrebare = new Intrebare("ce este?","1)2","2)3","3)3",1,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNull(intrebare);

    }

    @Test(expected = InputValidationFailedException.class)
    public void add3() throws Exception {
        Intrebare intrebare = new Intrebare("Ce este?","1)2","2)3","3)3",5,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNull(intrebare);

    }

    @Test(expected = InputValidationFailedException.class)
    public void add4() throws Exception {
        Intrebare intrebare = new Intrebare("","1)2","2)3","3)3",5,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNull(intrebare);
    }

    @Test
    public void add5() throws Exception {
        Intrebare intrebare = new Intrebare("Ce esteddddddddddddddddddddddddddddd?","1)2","2)3","3)3",1,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNotNull(intrebare);
    }

    @Test
    public void add6() throws Exception {
        Intrebare intrebare = new Intrebare("Ce esteddddddddddddddddddddddddddddds?","1)2","2)3","3)3",1,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNotNull(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void add7() throws Exception {
        Intrebare intrebare = new Intrebare("Ce esteddddddddddddddddddddddddddddds?","1)2","2)3","3)3",0,"Fizica");
        intrebariController.addNewIntrebare(intrebare);
        assertNull(intrebare);
    }
}