
package reir2180MV;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import reir2180MV.controller.IntrebariController;
import reir2180MV.exception.DuplicateIntrebareException;
import reir2180MV.exception.InputValidationFailedException;
import reir2180MV.exception.NotAbleToCreateStatisticsException;
import reir2180MV.exception.NotAbleToCreateTestException;
import reir2180MV.model.Intrebare;
import reir2180MV.model.Statistica;

import java.io.IOException;

import static org.junit.Assert.*;

public class BigBangTest {

    private IntrebariController intrebariController;

    @Before
    public void setUp() throws Exception {
        intrebariController = new IntrebariController();
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void addIntrebareTest1() throws Exception {
        try {
            intrebariController.addNewIntrebare(new Intrebare("De la ce firma este Note10 ?", "1) Nokia", "2) Samsung", "3) Apple", 2, "Telefoane"));
        } catch (InputValidationFailedException ex) {

        }
    }

    @Test
    public void createTestTest2() throws Exception {
        IntrebariController controller = new IntrebariController("src/main/resources/intrebari.txt");
        reir2180MV.model.Test test = controller.createNewTest();
        assertEquals(test.getIntrebari().size(), 5);
    }

    @Test
    public void createStatisticaTest1() throws Exception {
        IntrebariController controller = new IntrebariController("src/main/resources/intrebari.txt");
        Statistica statistica = controller.getStatistica();
        assert (statistica.getIntrebariDomenii().size() == 11);
    }

    @Test
    public void integrationTest() throws InputValidationFailedException, DuplicateIntrebareException, IOException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        try {
            intrebariController.addNewIntrebare(new Intrebare("De la ce firma este Note10 ?", "1) Nokia", "2) Samsung", "3) Apple", 2, "Telefoane"));
            intrebariController.addNewIntrebare(new Intrebare("De la ce firma este IphoneX ?", "1) Nokia", "2) Samsung", "3) Apple", 3, "Telefoane"));

            intrebariController.loadQuestionsFromFile("src/main/resources/intrebari.txt");
            reir2180MV.model.Test test = intrebariController.createNewTest();
            assertEquals(test.getIntrebari().size(), 5);

            intrebariController.loadQuestionsFromFile("src/main/resources/intrebari.txt");
            Statistica statistica = intrebariController.getStatistica();
            assert (statistica.getIntrebariDomenii().size() == 11);
        }
        catch (InputValidationFailedException ex) {

        }

    }
}